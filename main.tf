/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

resource "google_compute_instance_template" "default" {
  project     = "${var.project}"
  name_prefix = "default-"

  machine_type = "${var.machine_type}"

  region = "${var.region}"

  tags = "${var.target_tags}"

  labels = "${var.instance_labels}"

  network_interface {
    network            = "${var.subnetwork == "" ? var.network : ""}"
    subnetwork         = "${var.subnetwork}"
    subnetwork_project = "${var.subnetwork_project == "" ? var.project : var.subnetwork_project}"
  }

  can_ip_forward = "${var.can_ip_forward}"

  disk {
    auto_delete  = "${var.disk_auto_delete}"
    boot         = true
    source_image = "${var.compute_image}"
    type         = "PERSISTENT"
    disk_type    = "${var.disk_type}"
    disk_size_gb = "${var.disk_size_gb}"
    mode         = "${var.mode}"
  }

  service_account {
    email  = "${var.service_account_email}"
    scopes = "${var.service_account_scopes}"
  }

  metadata = "${merge(
    map("startup-script", "${var.startup_script}", "tf_depends_id", "${var.depends_id}"),
    var.metadata
  )}"

  scheduling {
    preemptible       = "${! var.preemptible}"
    automatic_restart = "${var.automatic_restart}"
  }

  lifecycle {
    create_before_destroy = true
  }
}
resource "google_compute_autoscaler" "default" {
  count   = "${var.autoscaling}"
  name    = "${var.name}"
  zone    = "${var.zone}"
  project = "${var.project}"
  target  = "${google_compute_region_instance_group_manager.default.self_link}"

  autoscaling_policy = {
    max_replicas               = "${var.max_replicas}"
    min_replicas               = "${var.min_replicas}"
    cooldown_period            = "${var.cooldown_period}"
    cpu_utilization            = ["${var.autoscaling_cpu}"]
    metric                     = ["${var.autoscaling_metric}"]
    load_balancing_utilization = ["${var.autoscaling_lb}"]
  }
}

data "google_compute_zones" "available" {
  project = "${var.project}"
  region  = "${var.region}"
}

locals {
  distribution_zones = {
    default = ["${data.google_compute_zones.available.names}"]
    user    = ["${var.distribution_policy_zones}"]
  }

  dependency_id = "${element(concat(null_resource.region_dummy_dependency.*.id, list("disabled")), 0)}"
}

resource "google_compute_region_instance_group_manager" "default" {
  project            = "${var.project}"
  provider = "google-beta"
  name               = "${var.name}"
  description        = "Region managed instance group. Created by Terraform"
  wait_for_instances = "${var.wait_for_instances}"

  base_instance_name = "${var.name}"

  version {
    name = "default"
    instance_template = "${google_compute_instance_template.default.self_link}"
  }
  
  region = "${var.region}"

  update_policy = "${list(var.rolling_update_policy)}"

  // There is no way to unset target_size when autoscaling is true so for now, jsut use the min_replicas value.
  // Issue: https://github.com/terraform-providers/terraform-provider-google/issues/667
  target_size = "${var.autoscaling ? var.min_replicas : var.size}"
}

resource "google_compute_region_autoscaler" "default" {
  count   = "${var.autoscaling}"
  name    = "${var.name}"
  region  = "${var.region}"
  project = "${var.project}"
  target  = "${google_compute_region_instance_group_manager.default.self_link}"

  autoscaling_policy = {
    max_replicas               = "${var.max_replicas}"
    min_replicas               = "${var.min_replicas}"
    cooldown_period            = "${var.cooldown_period}"
    cpu_utilization            = ["${var.autoscaling_cpu}"]
    metric                     = ["${var.autoscaling_metric}"]
    load_balancing_utilization = ["${var.autoscaling_lb}"]
  }
}

resource "null_resource" "region_dummy_dependency" {
  depends_on = ["google_compute_region_instance_group_manager.default"]

  triggers = {
    instance_template = "${element(google_compute_instance_template.default.*.self_link, 0)}"
  }
}

resource "google_compute_health_check" "mig-health-check" {
  count   = "${var.module_enabled && var.http_health_check ? 1 : 0}"
  name    = "${var.name}"
  project = "${var.project}"

  check_interval_sec  = "${var.hc_interval}"
  timeout_sec         = "${var.hc_timeout}"
  healthy_threshold   = "${var.hc_healthy_threshold}"
  unhealthy_threshold = "${var.hc_unhealthy_threshold}"

  http_health_check {
    port         = "${var.hc_port == "" ? var.service_port : var.hc_port}"
    request_path = "${var.hc_path}"
  }
}
